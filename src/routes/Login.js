import React, { Component } from 'react';
import axios from 'axios';
import { Form, Icon, Input, Button, message } from 'antd';
import './Login.css';

const FormItem = Form.Item;

class Login extends Component {
	submit = (e) => {
		e.preventDefault();

		this.props.form.validateFields((err, values) => {
      if (!err) {
        const { userName, password } = values;
        axios.get(`http://localhost:5000/api/user?userName=${userName}&password=${password}`)
	        .then(res => {
	        	this.props.signIn(res.data);
	        	this.props.history.push("/main");
	        })
	        .catch(err => {
	        	if(err.response.status === 401) {
	        		message.error(err.response.data);
	        		return;
						}
	        	console.error(err);
					});
      }
    });
	};

	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<div className="Login">
				<Form className="LoginForm" onSubmit={this.submit}>
					<FormItem>
						{getFieldDecorator('userName', {
							rules: [{ required: true, message: 'Please input your username!' }],
						})(
							<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>} placeholder="Username"/>
						)}
					</FormItem>
					<FormItem>
						{getFieldDecorator('password', {
							rules: [{ required: true, message: 'Please input your Password!' }],
						})(
							<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }}/>} type="password" placeholder="Password"/>
						)}
					</FormItem>
					<Button id="loginBT" type="primary" htmlType="submit">
            Log in
          </Button>
				</Form>
			</div>
		);
	}
}

Login = Form.create()(Login);

export default Login;