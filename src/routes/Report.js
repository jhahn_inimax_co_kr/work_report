import React, { Component } from 'react';
import UserInfo from "../component/UserInfo";
import WorkContentList from "../component/WorkContentList";
import WorkContent from "../component/WorkContent";
import MailForm from "../component/MailForm";
import { Button, message, Modal } from "antd";
import { Redirect } from 'react-router-dom';
import axios from "axios";
import './Report.css';

let reportTitle = '';

class Report extends Component {
	state = {
		online: false,
		workList: [],
		updateItem: false,
		useTime: false,
		itemIndex: null,
		projectName: null,
		startTime: null,
		endTime: null,
		content: null,
		mailTitle: '',
		mailContent: ''
	};

	// 리스트에 아이템 추가
	addToList = (data) => {
		const list = this.state.workList;
		const index = list.length;
		let actionType = '';

		// 아이템 수정인지 확인
		if (this.state.updateItem) {
			data.index = data.itemIndex;
			list[data.itemIndex] = data;
			actionType = '수정';
		} else {
			data.index = index;
			list.push(data);
			actionType = '추가';
		}

		this.setState({
			workList: list,
			updateItem: false
		});

		message.success(`[ ${data.project} ] 작업 내역 ${actionType}됨`);
	};

	updateItem = (idx, data) => {
		let useTime = false;
		let startTime = null;
		let endTime = null;
		if (data.time !== '-') {
			const timeData = data.time.split(' ~ ');
			useTime = true;
			startTime = timeData[0];
			endTime = timeData[1];
		}

		this.setState({
			updateItem: true,
			useTime: useTime,
			itemIndex: idx,
			projectName: data.project,
			startTime: startTime,
			endTime: endTime,
			content: data.description
		})
	};

	saveMailForm = (title, content) => {
		this.setState({
			mailTitle: title,
			mailContent: content
		});

		message.success(`Mail Form Saved`);
	};

	// FIXME 워드 문서 작성
	createWordFile = () => {
		axios.post('http://localhost:5000/api/docx', {
			title: reportTitle,
			workList: this.state.workList
		}).then(res => {
				if(!res.data) {
					message.error('fail in create docx file');
					return;
				}
				this.sendMailUsingOutlook();
			})
			.catch(err => console.error(err));
	};

	// FIXME 메일 발송
	sendMailUsingOutlook = () => {
	  console.log('메일 발송');
	};

	// 메일 보내기
	sendMail = () => {
		const _this = this;
		Modal.confirm({
			title: '메일을 보내시겠습니까?',
			content: '',
			okText: 'Send',
			cancelText: 'Cancel',
			onOk() {
				if(_this.state.workList.length === 0) {
					message.error('Work List is empty');
					return;
				}
				_this.createWordFile();
			}
		});
	};

	render() {
		const { name, email, projects, today, signOut } = this.props;
		const { workList, updateItem, useTime, itemIndex, projectName, startTime, endTime, content } = this.state;
		if (!name) {
			return <Redirect to='/login'/>;
		}

		reportTitle = `${today.format("YYYYMMDD")}_업무일지(${name})`;
		return (
			<div className="Report">
				<header>
					<h1>Business Log</h1>
					{/*사용자 정보*/}
					<UserInfo name={name} email={email} signOut={signOut}/>
				</header>
				{/*추가된 작업내용*/}
				<WorkContentList workList={workList} updateItem={this.updateItem}/>

				{/*작업내용 입력*/}
				<WorkContent
					projects={projects}
					addToList={this.addToList}
					updateItem={updateItem}
					useTime={useTime}
					itemIndex={itemIndex}
					projectName={projectName}
					startTime={startTime}
					endTime={endTime}
					content={content}
				/>

				{/*메일폼*/}
				<MailForm
					today={today}
					name={name}
					saveMailForm={this.saveMailForm}
				/>
				{/*전송 버튼*/}
				<Button className="sendBtn" type="primary" icon="rocket" onClick={this.sendMail}>SEND</Button>
			</div>
		)
	}
}

export default Report;