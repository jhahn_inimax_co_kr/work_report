const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 5000;

// CROS?
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
// to support JSON-encoded bodies
app.use(bodyParser.json());
// to support URL-encoded bodies
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api/user', function (req, res) {
  const userName = req.query.userName;
  const password = req.query.password;

  if (userName !== 'inimax' || password !== 'inimax') {
    res.status(401).send('Invalid username or password');
  } else {
    res.send({
      auth: true,
      name: '홍길동',
      email: 'sample@sample.com',
      projects: ['S2DDSv2.10', 'S2DDSv2.11', 'S2DDSv2.12', 'iDHCP', 'PMNAC', 'catch-B', '기타']
    });
  }
});

app.post('/api/docx', function (req, res) {
  res.send(true);
});

app.post('/api/mail', function (req, res) {
  res.send(true);
});

app.listen(port, function () {
  console.log('Example app listening on port', port);
});