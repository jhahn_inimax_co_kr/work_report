import React, { Component } from 'react';
import { Popover, Avatar, Modal } from 'antd';
import './UserInfo.css';

class UserInfo extends Component {
	logout = () => {
		const _this = this;
		Modal.confirm({
			title: '로그아웃 하시겠습니까?',
			content: '로그아웃시 로그인 페이지로 이동합니다.',
			okText: 'Logout',
			cancelText: 'Cancel',
			onOk() {
				_this.props.signOut();
			}
		});
	};

	render() {
		const { name, email } = this.props;
		const content = (
			<div>
				<p>{name}</p>
				<p>{email}</p>
			</div>
		);

		return (
			<div className="userInfo">
				<Popover content={content} title="User Info">
					<Avatar className="userInfoIcon" icon="user" style={{marginRight: '15px', backgroundColor: '#7b7b7b'}} />
				</Popover>
				<Avatar className="logoutIcon" icon="logout" onClick={() => this.logout()} style={{backgroundColor: '#7b7b7b', cursor: 'pointer'}} />
			</div>
		);
	}
}

export default UserInfo;