import React, { Component } from 'react';
import { List, Icon, notification, Modal } from 'antd';
import './WorkContentList.css';

class WorkContentList extends Component {
	state = {
		workList: this.props.workList
	};

	// 아이템 수정
	modifyReport = (idx) => {
		const _this = this;
		Modal.confirm({
	    title: '수정하시겠습니까?',
	    content: '',
	    okText: 'Modify',
	    cancelText: 'Cancel',
			onOk() {
				const item = _this.props.workList[idx];
				_this.props.updateItem(idx, item);
			}
	  });
	};

	// 더보기
	showMore = (idx) => {
		const item = this.props.workList[idx];
		notification.open({
			className: 'workContentNotify',
	    message: `${item.project} (${item.time})`,
	    description: item.description,
	    icon: <Icon type="inbox" />,
			duration: 0
	  });
	};

	// 아이템 삭제
	deleteItem = (idx) => {
		const _this = this;

		Modal.confirm({
	    title: '삭제하시겠습니까?',
	    content: '',
	    okText: 'Delete',
	    cancelText: 'Cancel',
			onOk() {
				const list = _this.state.workList;
				list.splice(idx, 1);
				_this.setState({
					workList: list
				})
			}
	  });
	};

	render() {
		const { workList } = this.props;

		return (
			<div className="workContentList">
				<h2>
					<Icon type="profile" style={{marginRight: 10}}/>Work List
				</h2>
				<List className="workList"
					itemLayout="horizontal"
					pagination={{ pageSize: 4 }}
					dataSource={workList}
					renderItem={item => (
			      <List.Item key={item.index}
			                 actions={[
				                <Icon type="ellipsis" onClick={() => this.showMore(item.index)}/>,
			                 	<Icon type="edit" onClick={() => this.modifyReport(item.index)}/>,
				                <Icon type="delete" onClick={() => this.deleteItem(item.index)}/>
			                 ]}>
				      {/*프로젝트명과 시간*/}
				      <List.Item.Meta title={item.project} description={item.time}/>
				      {/*내용*/}
				      {item.description}
			      </List.Item>
		      )}
				/>
			</div>
		);
	}
}

export default WorkContentList;