import React, { Component } from 'react';
import { Icon, Input, Button, message } from 'antd';
import './MailForm.css';

const { TextArea } = Input;

// 요일 가져오기
const getDayOfWeek = (num) => {
  let str = '';
  switch(num) {
    case 1:
      str = '월';
      break;
    case 2:
      str = '화';
      break;
    case 3:
      str = '수';
      break;
    case 4:
      str = '목';
      break;
    case 5:
      str = '금';
      break;
    case 6:
      str = '토';
      break;
    case 7:
      str = '일';
      break;
  }
  return str;
};

class MailForm extends Component {
	state = {
		showModifyBtn: true,
		showSaveBtn: false,
		disableModify: true,
		title: '',
		content: ''
	};

	titleChange = (val) => {
		this.setState({
			title: val
		})
	};

	contentChange = (val) => {
		this.setState({
			content: val
		})
	};

	modifyForm = () => {
		this.setState({
			showModifyBtn: false,
			showSaveBtn: true,
			disableModify: false
		})
	};

	saveForm = () => {
		const { title, content } = this.state;
		if(!title) {
			message.error('Mail Title is Empty');
			return;
		}
		if(!content) {
			message.error('Mail Contents is Empty');
			return;
		}

		this.setState({
			showModifyBtn: true,
			showSaveBtn: false,
			disableModify: true
		});

		this.props.saveMailForm(title, content);
	};

	render() {
		const { name, today } = this.props;
		// 메일 제목
		const mailTitle = `개발팀 업무일지(${name}) - ${today.format("YYYYMMDD")}`;
		// 메일 내용
		let mailContent = `안녕하세요. 개발팀 ${name}입니다.\r`
		+ `${today.format("YYYY-MM-DD")}(${getDayOfWeek(today.day())}) 업무일지 파일입니다.\r`
		+ `감사합니다.`;

		return (
			<div className="mailForm">
				<h2 style={{marginTop: 15}}>
          <Icon type="message" style={{marginRight: 10}}/>Mail Form
        </h2>
				<div className="mailFormWrap">
					{/*메일 제목*/}
					<Input className="mailTitle" defaultValue={mailTitle} disabled={this.state.disableModify} onChange={(e) => this.titleChange(e.target.value)}/>
					{/*메일 내용*/}
					<div className="mailContent">
						<TextArea defaultValue={mailContent} rows={3} disabled={this.state.disableModify} onChange={(e) => this.contentChange(e.target.value)}/>
					</div>
					{/*버튼*/}
					<div className="mailBtnWrap">
						<Button icon="edit" onClick={this.modifyForm} style={{display: this.state.showModifyBtn ? 'block': 'none'}}>
							Modify
						</Button>
						<Button icon="save" onClick={this.saveForm} style={{display: this.state.showSaveBtn ? 'block' : 'none'}}>
							Save
						</Button>
						<Icon/>
					</div>
				</div>
			</div>
		);
	}
}

export default MailForm;